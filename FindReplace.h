#pragma once

namespace Nopepad
{

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// Summary for FindReplace
	/// </summary>
	public ref class FindReplace : public System::Windows::Forms::Form
	{
		public:
		FindReplace(void)
		{
			InitializeComponent();
			//
			//TODO: Add the constructor code here
			//
		}

		protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~FindReplace()
		{
			if (components)
			{
				delete components;
			}
		}
		private: System::Windows::Forms::Label^  label1;
		protected:

		private: System::Windows::Forms::Button^  btnCancel;
		private: System::Windows::Forms::Button^  btnFindNext;
		private: System::Windows::Forms::TextBox^  tbFindWhat;


		private: System::Windows::Forms::CheckBox^  cbMatchCase;
		private: System::Windows::Forms::Button^  btnReplace;
		private: System::Windows::Forms::Button^  btnReplaceAll;
		private: System::Windows::Forms::Label^  label2;
		private: System::Windows::Forms::TextBox^  tbReplace;


		private:
		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container ^components;

		#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->btnCancel = (gcnew System::Windows::Forms::Button());
			this->btnFindNext = (gcnew System::Windows::Forms::Button());
			this->tbFindWhat = (gcnew System::Windows::Forms::TextBox());
			this->cbMatchCase = (gcnew System::Windows::Forms::CheckBox());
			this->btnReplace = (gcnew System::Windows::Forms::Button());
			this->btnReplaceAll = (gcnew System::Windows::Forms::Button());
			this->label2 = (gcnew System::Windows::Forms::Label());
			this->tbReplace = (gcnew System::Windows::Forms::TextBox());
			this->SuspendLayout();
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->Location = System::Drawing::Point(9, 12);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(59, 13);
			this->label1->TabIndex = 6;
			this->label1->Text = L"Find what: ";
			// 
			// btnCancel
			// 
			this->btnCancel->Location = System::Drawing::Point(318, 100);
			this->btnCancel->Name = L"btnCancel";
			this->btnCancel->Size = System::Drawing::Size(75, 23);
			this->btnCancel->TabIndex = 9;
			this->btnCancel->Text = L"Cancel";
			this->btnCancel->UseVisualStyleBackColor = true;
			this->btnCancel->Click += gcnew System::EventHandler(this, &FindReplace::btnCancel_Click);
			// 
			// btnFindNext
			// 
			this->btnFindNext->Enabled = false;
			this->btnFindNext->Location = System::Drawing::Point(318, 11);
			this->btnFindNext->Name = L"btnFindNext";
			this->btnFindNext->Size = System::Drawing::Size(75, 23);
			this->btnFindNext->TabIndex = 8;
			this->btnFindNext->Text = L"Find Next";
			this->btnFindNext->UseVisualStyleBackColor = true;
			this->btnFindNext->Click += gcnew System::EventHandler(this, &FindReplace::btnFindNext_Click);
			// 
			// tbFindWhat
			// 
			this->tbFindWhat->Location = System::Drawing::Point(93, 12);
			this->tbFindWhat->Name = L"tbFindWhat";
			this->tbFindWhat->Size = System::Drawing::Size(219, 20);
			this->tbFindWhat->TabIndex = 7;
			this->tbFindWhat->TextChanged += gcnew System::EventHandler(this, &FindReplace::tbFindWhat_TextChanged);
			// 
			// cbMatchCase
			// 
			this->cbMatchCase->AutoSize = true;
			this->cbMatchCase->Location = System::Drawing::Point(12, 104);
			this->cbMatchCase->Name = L"cbMatchCase";
			this->cbMatchCase->Size = System::Drawing::Size(83, 17);
			this->cbMatchCase->TabIndex = 10;
			this->cbMatchCase->Text = L"Match Case";
			this->cbMatchCase->UseVisualStyleBackColor = true;
			// 
			// btnReplace
			// 
			this->btnReplace->Location = System::Drawing::Point(318, 41);
			this->btnReplace->Name = L"btnReplace";
			this->btnReplace->Size = System::Drawing::Size(75, 23);
			this->btnReplace->TabIndex = 12;
			this->btnReplace->Text = L"Replace";
			this->btnReplace->UseVisualStyleBackColor = true;
			this->btnReplace->Click += gcnew System::EventHandler(this, &FindReplace::btnReplace_Click);
			// 
			// btnReplaceAll
			// 
			this->btnReplaceAll->Location = System::Drawing::Point(318, 71);
			this->btnReplaceAll->Name = L"btnReplaceAll";
			this->btnReplaceAll->Size = System::Drawing::Size(75, 23);
			this->btnReplaceAll->TabIndex = 13;
			this->btnReplaceAll->Text = L"Replace All";
			this->btnReplaceAll->UseVisualStyleBackColor = true;
			this->btnReplaceAll->Click += gcnew System::EventHandler(this, &FindReplace::btnReplaceAll_Click);
			// 
			// label2
			// 
			this->label2->AutoSize = true;
			this->label2->Location = System::Drawing::Point(12, 46);
			this->label2->Name = L"label2";
			this->label2->Size = System::Drawing::Size(75, 13);
			this->label2->TabIndex = 14;
			this->label2->Text = L"Replace with: ";
			// 
			// tbReplace
			// 
			this->tbReplace->Location = System::Drawing::Point(93, 43);
			this->tbReplace->Name = L"tbReplace";
			this->tbReplace->Size = System::Drawing::Size(219, 20);
			this->tbReplace->TabIndex = 15;
			this->tbReplace->TextChanged += gcnew System::EventHandler(this, &FindReplace::tbReplace_TextChanged);
			// 
			// FindReplace
			// 
			this->AcceptButton = this->btnFindNext;
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(403, 158);
			this->Controls->Add(this->tbReplace);
			this->Controls->Add(this->label2);
			this->Controls->Add(this->btnReplaceAll);
			this->Controls->Add(this->btnReplace);
			this->Controls->Add(this->label1);
			this->Controls->Add(this->btnCancel);
			this->Controls->Add(this->btnFindNext);
			this->Controls->Add(this->tbFindWhat);
			this->Controls->Add(this->cbMatchCase);
			this->Name = L"FindReplace";
			this->Text = L"Replace";
			this->ResumeLayout(false);
			this->PerformLayout();

		}
		#pragma endregion
		private: System::Void btnCancel_Click(System::Object^  sender, System::EventArgs^  e)
		{
			this->Close();
		}

		public: String^ searchText = "";
		public: int mode = 0; //0: search, 1: replace, 2: replace all

		private: System::Void btnFindNext_Click(System::Object^  sender, System::EventArgs^  e)
		{
			mode = 0;
			searchText = tbFindWhat->Text;
		}

		private: System::Void btnReplace_Click(System::Object^  sender, System::EventArgs^  e)
		{
			mode = 1;
			searchText = tbFindWhat->Text;
		}

		private: System::Void btnReplaceAll_Click(System::Object^  sender, System::EventArgs^  e)
		{
			mode = 2;
			searchText = tbFindWhat->Text;
		}

		private: System::Void tbFindWhat_TextChanged(System::Object^  sender, System::EventArgs^  e)
		{
			btnFindNext->Enabled = !String::IsNullOrEmpty(tbFindWhat->Text);
		}
		private: System::Void tbReplace_TextChanged(System::Object^  sender, System::EventArgs^  e)
		{
			bool b = (!String::IsNullOrEmpty(tbFindWhat->Text) && !String::IsNullOrEmpty(tbReplace->Text));
			btnReplace->Enabled = b;
			btnReplaceAll->Enabled = b;
		}
	};
}
