#pragma once

namespace Nopepad
{
	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// Summary for Find
	/// </summary>
	public ref class Find : public System::Windows::Forms::Form
	{
		public:
		Find(void)
		{
			InitializeComponent();
			//
			//TODO: Add the constructor code here
			//
		}

		protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~Find()
		{
			if (components)
			{
				delete components;
			}
		}
		private: System::Windows::Forms::Label^  label1;
		protected:
		private: System::Windows::Forms::TextBox^  tbFindWhat;
		private: System::Windows::Forms::Button^  btnFindNext;
		private: System::Windows::Forms::Button^  btnCancel;
		public: System::Windows::Forms::CheckBox^  cbMatchCase;
		private:

		private: System::Windows::Forms::GroupBox^  groupBox1;
		public: System::Windows::Forms::RadioButton^  rbDown;
		private: System::Windows::Forms::RadioButton^  rbUp;

		private:
		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container ^components;

		#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->tbFindWhat = (gcnew System::Windows::Forms::TextBox());
			this->btnFindNext = (gcnew System::Windows::Forms::Button());
			this->btnCancel = (gcnew System::Windows::Forms::Button());
			this->cbMatchCase = (gcnew System::Windows::Forms::CheckBox());
			this->groupBox1 = (gcnew System::Windows::Forms::GroupBox());
			this->rbDown = (gcnew System::Windows::Forms::RadioButton());
			this->rbUp = (gcnew System::Windows::Forms::RadioButton());
			this->groupBox1->SuspendLayout();
			this->SuspendLayout();
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->Location = System::Drawing::Point(13, 13);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(59, 13);
			this->label1->TabIndex = 0;
			this->label1->Text = L"Find what: ";
			// 
			// tbFindWhat
			// 
			this->tbFindWhat->Location = System::Drawing::Point(79, 13);
			this->tbFindWhat->Name = L"tbFindWhat";
			this->tbFindWhat->Size = System::Drawing::Size(237, 20);
			this->tbFindWhat->TabIndex = 1;
			this->tbFindWhat->TextChanged += gcnew System::EventHandler(this, &Find::tbFindWhat_TextChanged);
			// 
			// btnFindNext
			// 
			this->btnFindNext->Enabled = false;
			this->btnFindNext->Location = System::Drawing::Point(322, 12);
			this->btnFindNext->Name = L"btnFindNext";
			this->btnFindNext->Size = System::Drawing::Size(75, 23);
			this->btnFindNext->TabIndex = 2;
			this->btnFindNext->Text = L"Find Next";
			this->btnFindNext->UseVisualStyleBackColor = true;
			this->btnFindNext->Click += gcnew System::EventHandler(this, &Find::btnFindNext_Click);
			// 
			// btnCancel
			// 
			this->btnCancel->DialogResult = System::Windows::Forms::DialogResult::Cancel;
			this->btnCancel->Location = System::Drawing::Point(322, 42);
			this->btnCancel->Name = L"btnCancel";
			this->btnCancel->Size = System::Drawing::Size(75, 23);
			this->btnCancel->TabIndex = 3;
			this->btnCancel->Text = L"Cancel";
			this->btnCancel->UseVisualStyleBackColor = true;
			this->btnCancel->Click += gcnew System::EventHandler(this, &Find::btnCancel_Click);
			// 
			// cbMatchCase
			// 
			this->cbMatchCase->AutoSize = true;
			this->cbMatchCase->Location = System::Drawing::Point(12, 63);
			this->cbMatchCase->Name = L"cbMatchCase";
			this->cbMatchCase->Size = System::Drawing::Size(83, 17);
			this->cbMatchCase->TabIndex = 4;
			this->cbMatchCase->Text = L"Match Case";
			this->cbMatchCase->UseVisualStyleBackColor = true;
			// 
			// groupBox1
			// 
			this->groupBox1->Controls->Add(this->rbDown);
			this->groupBox1->Controls->Add(this->rbUp);
			this->groupBox1->Location = System::Drawing::Point(207, 42);
			this->groupBox1->Name = L"groupBox1";
			this->groupBox1->Size = System::Drawing::Size(109, 39);
			this->groupBox1->TabIndex = 5;
			this->groupBox1->TabStop = false;
			this->groupBox1->Text = L"Direction";
			// 
			// rbDown
			// 
			this->rbDown->AutoSize = true;
			this->rbDown->Checked = true;
			this->rbDown->Location = System::Drawing::Point(53, 20);
			this->rbDown->Name = L"rbDown";
			this->rbDown->Size = System::Drawing::Size(53, 17);
			this->rbDown->TabIndex = 1;
			this->rbDown->TabStop = true;
			this->rbDown->Text = L"Down";
			this->rbDown->UseVisualStyleBackColor = true;
			// 
			// rbUp
			// 
			this->rbUp->AutoSize = true;
			this->rbUp->Location = System::Drawing::Point(7, 20);
			this->rbUp->Name = L"rbUp";
			this->rbUp->Size = System::Drawing::Size(39, 17);
			this->rbUp->TabIndex = 0;
			this->rbUp->Text = L"Up";
			this->rbUp->UseVisualStyleBackColor = true;
			// 
			// Find
			// 
			this->AcceptButton = this->btnFindNext;
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->CancelButton = this->btnCancel;
			this->ClientSize = System::Drawing::Size(409, 96);
			this->Controls->Add(this->groupBox1);
			this->Controls->Add(this->cbMatchCase);
			this->Controls->Add(this->btnCancel);
			this->Controls->Add(this->btnFindNext);
			this->Controls->Add(this->tbFindWhat);
			this->Controls->Add(this->label1);
			this->FormBorderStyle = System::Windows::Forms::FormBorderStyle::FixedDialog;
			this->MaximizeBox = false;
			this->MinimizeBox = false;
			this->Name = L"Find";
			this->ShowIcon = false;
			this->ShowInTaskbar = false;
			this->Text = L"Find";
			this->groupBox1->ResumeLayout(false);
			this->groupBox1->PerformLayout();
			this->ResumeLayout(false);
			this->PerformLayout();

		}
		#pragma endregion

		private: System::Void tbFindWhat_TextChanged(System::Object^  sender, System::EventArgs^  e)
		{
			if (!String::IsNullOrEmpty(tbFindWhat->Text)) btnFindNext->Enabled = true;
			else btnFindNext->Enabled = false;
		}

		public: String^ searchText = "";

		private: System::Void btnFindNext_Click(System::Object^  sender, System::EventArgs^  e)
		{
			searchText = tbFindWhat->Text;
		}

		private: System::Void btnCancel_Click(System::Object^  sender, System::EventArgs^  e)
		{
			this->Close();
		}
};
}
