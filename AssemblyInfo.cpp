using namespace System;
using namespace System::Reflection;
using namespace System::Runtime::CompilerServices;
using namespace System::Runtime::InteropServices;
using namespace System::Security::Permissions;

[assembly:AssemblyTitleAttribute("Nopepad")];
[assembly:AssemblyDescriptionAttribute("")];
[assembly:AssemblyConfigurationAttribute("")];
[assembly:AssemblyCompanyAttribute("LetsPlayOrDy")];
[assembly:AssemblyProductAttribute("Nopepad")];
[assembly:AssemblyCopyrightAttribute("Copyright � 2016. LetsPlayOrDy.")];
[assembly:AssemblyTrademarkAttribute("")];
[assembly:AssemblyCultureAttribute("")];
[assembly:AssemblyVersionAttribute("1.0.0.0")];
[assembly:ComVisible(false)];
[assembly:CLSCompliantAttribute(true)];