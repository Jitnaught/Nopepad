#include "Main.h"
#include <Windows.h>

using namespace System;
using namespace System::Windows::Forms;

void Main(array<String^>^ args)
{
	Application::EnableVisualStyles();
	Application::SetCompatibleTextRenderingDefault(false);

	Nopepad::Main form;
	Application::Run(%form);
}

[STAThread]
int WINAPI _stdcall WinMain(_In_ HINSTANCE hInstance, _In_opt_ HINSTANCE hPrevInstance, _In_ LPSTR lpCmdLine, _In_ int nShowCmd)
{
	String^ strCmdLine = gcnew String(lpCmdLine);
	String^ delimStr = " ,.:\t";
	array<Char>^ delimiter = delimStr->ToCharArray();
	array<String^>^ words;
	words = strCmdLine->Split(delimiter);
	Main(words);

	return 0;
}