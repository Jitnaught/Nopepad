#pragma once

#include <Windows.h>
#include <string>
#include "About.h"
#include "Find.h"
#include "GoTo.h"
#include "NPHelp.h"
#include "Replace.h"

namespace Nopepad
{
	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Collections::Generic;
	using namespace System::Drawing::Printing;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;
	using namespace System::IO;
	using namespace System::Windows;
	using namespace System::Text;
	using namespace System::Threading;
	//using namespace HelpPane;

	/// <summary>
	/// Summary for Main
	/// </summary>
	public ref class Main : public System::Windows::Forms::Form
	{
		public:
		Main(void)
		{
			InitializeComponent();
		}

		protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~Main()
		{
			if (components)
			{
				delete components;
			}
		}
		#pragma region Windows Form Designer generated code
		public: System::Windows::Forms::RichTextBox^  richTextBox1;
		protected:
		private: System::Windows::Forms::MenuStrip^  menuStrip1;
		private: System::Windows::Forms::ToolStripMenuItem^  fileToolStripMenuItem;
		private: System::Windows::Forms::ToolStripMenuItem^  newToolStripMenuItem;
		private: System::Windows::Forms::ToolStripMenuItem^  openToolStripMenuItem;
		private: System::Windows::Forms::ToolStripMenuItem^  editToolStripMenuItem;
		private: System::Windows::Forms::ToolStripMenuItem^  formatToolStripMenuItem;
		private: System::Windows::Forms::ToolStripMenuItem^  viewToolStripMenuItem;
		private: System::Windows::Forms::ToolStripMenuItem^  helpToolStripMenuItem;
		private: System::Windows::Forms::ToolStripMenuItem^  saveToolStripMenuItem;
		private: System::Windows::Forms::ToolStripMenuItem^  saveAsToolStripMenuItem;
		private: System::Windows::Forms::ToolStripSeparator^  toolStripSeparator1;
		private: System::Windows::Forms::ToolStripMenuItem^  pageSetupToolStripMenuItem;
		private: System::Windows::Forms::ToolStripMenuItem^  printToolStripMenuItem;
		private: System::Windows::Forms::ToolStripSeparator^  toolStripSeparator2;
		private: System::Windows::Forms::ToolStripMenuItem^  exitToolStripMenuItem;
		private: System::Windows::Forms::ToolStripMenuItem^  undoToolStripMenuItem;
		private: System::Windows::Forms::ToolStripSeparator^  toolStripSeparator3;
		private: System::Windows::Forms::ToolStripMenuItem^  cutToolStripMenuItem;
		private: System::Windows::Forms::ToolStripMenuItem^  copyToolStripMenuItem;
		private: System::Windows::Forms::ToolStripMenuItem^  pasteToolStripMenuItem;
		private: System::Windows::Forms::ToolStripMenuItem^  deleteToolStripMenuItem;
		private: System::Windows::Forms::ToolStripSeparator^  toolStripSeparator4;
		private: System::Windows::Forms::ToolStripMenuItem^  findToolStripMenuItem;
		private: System::Windows::Forms::ToolStripMenuItem^  findNextToolStripMenuItem;
		private: System::Windows::Forms::ToolStripMenuItem^  replaceToolStripMenuItem;
		private: System::Windows::Forms::ToolStripMenuItem^  goToToolStripMenuItem;
		private: System::Windows::Forms::ToolStripSeparator^  toolStripSeparator5;
		private: System::Windows::Forms::ToolStripMenuItem^  selectAllToolStripMenuItem;
		private: System::Windows::Forms::ToolStripMenuItem^  timeDateToolStripMenuItem;
		private: System::Windows::Forms::ToolStripMenuItem^  wordWrapToolStripMenuItem;
		private: System::Windows::Forms::ToolStripMenuItem^  fontToolStripMenuItem;
		private: System::Windows::Forms::ToolStripMenuItem^  statusBarToolStripMenuItem;
		private: System::Windows::Forms::ToolStripMenuItem^  viewHelpToolStripMenuItem;
		private: System::Windows::Forms::ToolStripSeparator^  toolStripSeparator6;
		private: System::Windows::Forms::ToolStripMenuItem^  aboutNopepadToolStripMenuItem;
		private: System::Windows::Forms::StatusStrip^  statusStrip1;
		private: System::Windows::Forms::ToolStripStatusLabel^  toolStripStatusLabel1;
		private: System::Windows::Forms::Timer^  timer1;
		private: System::Windows::Forms::PageSetupDialog^  pageSetupDialog1;

		private: System::ComponentModel::IContainer^  components;

		private:
		/// <summary>
		/// Required designer variable.
		/// </summary>



		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->components = (gcnew System::ComponentModel::Container());
			this->richTextBox1 = (gcnew System::Windows::Forms::RichTextBox());
			this->menuStrip1 = (gcnew System::Windows::Forms::MenuStrip());
			this->fileToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->newToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->openToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->saveToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->saveAsToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->toolStripSeparator1 = (gcnew System::Windows::Forms::ToolStripSeparator());
			this->pageSetupToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->printToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->toolStripSeparator2 = (gcnew System::Windows::Forms::ToolStripSeparator());
			this->exitToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->editToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->undoToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->toolStripSeparator3 = (gcnew System::Windows::Forms::ToolStripSeparator());
			this->cutToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->copyToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->pasteToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->deleteToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->toolStripSeparator4 = (gcnew System::Windows::Forms::ToolStripSeparator());
			this->findToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->findNextToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->replaceToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->goToToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->toolStripSeparator5 = (gcnew System::Windows::Forms::ToolStripSeparator());
			this->selectAllToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->timeDateToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->formatToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->wordWrapToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->fontToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->viewToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->statusBarToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->helpToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->viewHelpToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->toolStripSeparator6 = (gcnew System::Windows::Forms::ToolStripSeparator());
			this->aboutNopepadToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->statusStrip1 = (gcnew System::Windows::Forms::StatusStrip());
			this->toolStripStatusLabel1 = (gcnew System::Windows::Forms::ToolStripStatusLabel());
			this->timer1 = (gcnew System::Windows::Forms::Timer(this->components));
			this->pageSetupDialog1 = (gcnew System::Windows::Forms::PageSetupDialog());
			this->menuStrip1->SuspendLayout();
			this->statusStrip1->SuspendLayout();
			this->SuspendLayout();
			// 
			// richTextBox1
			// 
			this->richTextBox1->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Bottom)
				| System::Windows::Forms::AnchorStyles::Left)
				| System::Windows::Forms::AnchorStyles::Right));
			this->richTextBox1->HideSelection = false;
			this->richTextBox1->Location = System::Drawing::Point(-2, 27);
			this->richTextBox1->Name = L"richTextBox1";
			this->richTextBox1->ScrollBars = System::Windows::Forms::RichTextBoxScrollBars::ForcedBoth;
			this->richTextBox1->Size = System::Drawing::Size(687, 352);
			this->richTextBox1->TabIndex = 0;
			this->richTextBox1->Text = L"";
			this->richTextBox1->WordWrap = false;
			this->richTextBox1->TextChanged += gcnew System::EventHandler(this, &Main::richTextBox1_TextChanged);
			// 
			// menuStrip1
			// 
			this->menuStrip1->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(5)
			{
				this->fileToolStripMenuItem,
					this->editToolStripMenuItem, this->formatToolStripMenuItem, this->viewToolStripMenuItem, this->helpToolStripMenuItem
			});
			this->menuStrip1->Location = System::Drawing::Point(0, 0);
			this->menuStrip1->Name = L"menuStrip1";
			this->menuStrip1->Size = System::Drawing::Size(684, 24);
			this->menuStrip1->TabIndex = 1;
			this->menuStrip1->Text = L"menuStrip1";
			// 
			// fileToolStripMenuItem
			// 
			this->fileToolStripMenuItem->DropDownItems->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(9)
			{
				this->newToolStripMenuItem,
					this->openToolStripMenuItem, this->saveToolStripMenuItem, this->saveAsToolStripMenuItem, this->toolStripSeparator1, this->pageSetupToolStripMenuItem,
					this->printToolStripMenuItem, this->toolStripSeparator2, this->exitToolStripMenuItem
			});
			this->fileToolStripMenuItem->Name = L"fileToolStripMenuItem";
			this->fileToolStripMenuItem->Size = System::Drawing::Size(37, 20);
			this->fileToolStripMenuItem->Text = L"File";
			// 
			// newToolStripMenuItem
			// 
			this->newToolStripMenuItem->Name = L"newToolStripMenuItem";
			this->newToolStripMenuItem->ShortcutKeyDisplayString = L"";
			this->newToolStripMenuItem->ShortcutKeys = static_cast<System::Windows::Forms::Keys>((System::Windows::Forms::Keys::Control | System::Windows::Forms::Keys::N));
			this->newToolStripMenuItem->Size = System::Drawing::Size(155, 22);
			this->newToolStripMenuItem->Text = L"New";
			this->newToolStripMenuItem->Click += gcnew System::EventHandler(this, &Main::newToolStripMenuItem_Click);
			// 
			// openToolStripMenuItem
			// 
			this->openToolStripMenuItem->Name = L"openToolStripMenuItem";
			this->openToolStripMenuItem->ShortcutKeyDisplayString = L"";
			this->openToolStripMenuItem->ShortcutKeys = static_cast<System::Windows::Forms::Keys>((System::Windows::Forms::Keys::Control | System::Windows::Forms::Keys::O));
			this->openToolStripMenuItem->Size = System::Drawing::Size(155, 22);
			this->openToolStripMenuItem->Text = L"Open...";
			this->openToolStripMenuItem->Click += gcnew System::EventHandler(this, &Main::openToolStripMenuItem_Click);
			// 
			// saveToolStripMenuItem
			// 
			this->saveToolStripMenuItem->Name = L"saveToolStripMenuItem";
			this->saveToolStripMenuItem->ShortcutKeyDisplayString = L"";
			this->saveToolStripMenuItem->ShortcutKeys = static_cast<System::Windows::Forms::Keys>((System::Windows::Forms::Keys::Control | System::Windows::Forms::Keys::S));
			this->saveToolStripMenuItem->Size = System::Drawing::Size(155, 22);
			this->saveToolStripMenuItem->Text = L"Save";
			this->saveToolStripMenuItem->Click += gcnew System::EventHandler(this, &Main::saveToolStripMenuItem_Click);
			// 
			// saveAsToolStripMenuItem
			// 
			this->saveAsToolStripMenuItem->Name = L"saveAsToolStripMenuItem";
			this->saveAsToolStripMenuItem->Size = System::Drawing::Size(155, 22);
			this->saveAsToolStripMenuItem->Text = L"Save As...";
			this->saveAsToolStripMenuItem->Click += gcnew System::EventHandler(this, &Main::saveAsToolStripMenuItem_Click);
			// 
			// toolStripSeparator1
			// 
			this->toolStripSeparator1->Name = L"toolStripSeparator1";
			this->toolStripSeparator1->Size = System::Drawing::Size(152, 6);
			// 
			// pageSetupToolStripMenuItem
			// 
			this->pageSetupToolStripMenuItem->Name = L"pageSetupToolStripMenuItem";
			this->pageSetupToolStripMenuItem->Size = System::Drawing::Size(155, 22);
			this->pageSetupToolStripMenuItem->Text = L"Page Setup...";
			this->pageSetupToolStripMenuItem->Click += gcnew System::EventHandler(this, &Main::pageSetupToolStripMenuItem_Click);
			// 
			// printToolStripMenuItem
			// 
			this->printToolStripMenuItem->Name = L"printToolStripMenuItem";
			this->printToolStripMenuItem->ShortcutKeyDisplayString = L"";
			this->printToolStripMenuItem->ShortcutKeys = static_cast<System::Windows::Forms::Keys>((System::Windows::Forms::Keys::Control | System::Windows::Forms::Keys::P));
			this->printToolStripMenuItem->Size = System::Drawing::Size(155, 22);
			this->printToolStripMenuItem->Text = L"Print...";
			this->printToolStripMenuItem->Click += gcnew System::EventHandler(this, &Main::printToolStripMenuItem_Click);
			// 
			// toolStripSeparator2
			// 
			this->toolStripSeparator2->Name = L"toolStripSeparator2";
			this->toolStripSeparator2->Size = System::Drawing::Size(152, 6);
			// 
			// exitToolStripMenuItem
			// 
			this->exitToolStripMenuItem->Name = L"exitToolStripMenuItem";
			this->exitToolStripMenuItem->Size = System::Drawing::Size(155, 22);
			this->exitToolStripMenuItem->Text = L"Exit";
			this->exitToolStripMenuItem->Click += gcnew System::EventHandler(this, &Main::exitToolStripMenuItem_Click);
			// 
			// editToolStripMenuItem
			// 
			this->editToolStripMenuItem->DropDownItems->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(14)
			{
				this->undoToolStripMenuItem,
					this->toolStripSeparator3, this->cutToolStripMenuItem, this->copyToolStripMenuItem, this->pasteToolStripMenuItem, this->deleteToolStripMenuItem,
					this->toolStripSeparator4, this->findToolStripMenuItem, this->findNextToolStripMenuItem, this->replaceToolStripMenuItem, this->goToToolStripMenuItem,
					this->toolStripSeparator5, this->selectAllToolStripMenuItem, this->timeDateToolStripMenuItem
			});
			this->editToolStripMenuItem->Name = L"editToolStripMenuItem";
			this->editToolStripMenuItem->Size = System::Drawing::Size(39, 20);
			this->editToolStripMenuItem->Text = L"Edit";
			// 
			// undoToolStripMenuItem
			// 
			this->undoToolStripMenuItem->Enabled = false;
			this->undoToolStripMenuItem->Name = L"undoToolStripMenuItem";
			this->undoToolStripMenuItem->ShortcutKeyDisplayString = L"Ctrl+Z";
			this->undoToolStripMenuItem->Size = System::Drawing::Size(167, 22);
			this->undoToolStripMenuItem->Text = L"Undo";
			this->undoToolStripMenuItem->Click += gcnew System::EventHandler(this, &Main::undoToolStripMenuItem_Click);
			// 
			// toolStripSeparator3
			// 
			this->toolStripSeparator3->Name = L"toolStripSeparator3";
			this->toolStripSeparator3->Size = System::Drawing::Size(164, 6);
			// 
			// cutToolStripMenuItem
			// 
			this->cutToolStripMenuItem->Enabled = false;
			this->cutToolStripMenuItem->Name = L"cutToolStripMenuItem";
			this->cutToolStripMenuItem->ShortcutKeyDisplayString = L"Ctrl+X";
			this->cutToolStripMenuItem->Size = System::Drawing::Size(167, 22);
			this->cutToolStripMenuItem->Text = L"Cut";
			this->cutToolStripMenuItem->Click += gcnew System::EventHandler(this, &Main::cutToolStripMenuItem_Click);
			// 
			// copyToolStripMenuItem
			// 
			this->copyToolStripMenuItem->Enabled = false;
			this->copyToolStripMenuItem->Name = L"copyToolStripMenuItem";
			this->copyToolStripMenuItem->ShortcutKeyDisplayString = L"Ctrl+C";
			this->copyToolStripMenuItem->Size = System::Drawing::Size(167, 22);
			this->copyToolStripMenuItem->Text = L"Copy";
			this->copyToolStripMenuItem->Click += gcnew System::EventHandler(this, &Main::copyToolStripMenuItem_Click);
			// 
			// pasteToolStripMenuItem
			// 
			this->pasteToolStripMenuItem->Enabled = false;
			this->pasteToolStripMenuItem->Name = L"pasteToolStripMenuItem";
			this->pasteToolStripMenuItem->ShortcutKeyDisplayString = L"Ctrl+V";
			this->pasteToolStripMenuItem->Size = System::Drawing::Size(167, 22);
			this->pasteToolStripMenuItem->Text = L"Paste";
			this->pasteToolStripMenuItem->Click += gcnew System::EventHandler(this, &Main::pasteToolStripMenuItem_Click);
			// 
			// deleteToolStripMenuItem
			// 
			this->deleteToolStripMenuItem->Enabled = false;
			this->deleteToolStripMenuItem->Name = L"deleteToolStripMenuItem";
			this->deleteToolStripMenuItem->ShortcutKeyDisplayString = L"Del";
			this->deleteToolStripMenuItem->Size = System::Drawing::Size(167, 22);
			this->deleteToolStripMenuItem->Text = L"Delete";
			this->deleteToolStripMenuItem->Click += gcnew System::EventHandler(this, &Main::deleteToolStripMenuItem_Click);
			// 
			// toolStripSeparator4
			// 
			this->toolStripSeparator4->Name = L"toolStripSeparator4";
			this->toolStripSeparator4->Size = System::Drawing::Size(164, 6);
			// 
			// findToolStripMenuItem
			// 
			this->findToolStripMenuItem->Enabled = false;
			this->findToolStripMenuItem->Name = L"findToolStripMenuItem";
			this->findToolStripMenuItem->ShortcutKeyDisplayString = L"";
			this->findToolStripMenuItem->ShortcutKeys = static_cast<System::Windows::Forms::Keys>((System::Windows::Forms::Keys::Control | System::Windows::Forms::Keys::F));
			this->findToolStripMenuItem->Size = System::Drawing::Size(167, 22);
			this->findToolStripMenuItem->Text = L"Find...";
			this->findToolStripMenuItem->Click += gcnew System::EventHandler(this, &Main::findToolStripMenuItem_Click);
			// 
			// findNextToolStripMenuItem
			// 
			this->findNextToolStripMenuItem->Enabled = false;
			this->findNextToolStripMenuItem->Name = L"findNextToolStripMenuItem";
			this->findNextToolStripMenuItem->ShortcutKeyDisplayString = L"";
			this->findNextToolStripMenuItem->ShortcutKeys = System::Windows::Forms::Keys::F3;
			this->findNextToolStripMenuItem->Size = System::Drawing::Size(167, 22);
			this->findNextToolStripMenuItem->Text = L"Find Next";
			this->findNextToolStripMenuItem->Click += gcnew System::EventHandler(this, &Main::findNextToolStripMenuItem_Click);
			// 
			// replaceToolStripMenuItem
			// 
			this->replaceToolStripMenuItem->Name = L"replaceToolStripMenuItem";
			this->replaceToolStripMenuItem->ShortcutKeyDisplayString = L"";
			this->replaceToolStripMenuItem->ShortcutKeys = static_cast<System::Windows::Forms::Keys>((System::Windows::Forms::Keys::Control | System::Windows::Forms::Keys::H));
			this->replaceToolStripMenuItem->Size = System::Drawing::Size(167, 22);
			this->replaceToolStripMenuItem->Text = L"Replace...";
			this->replaceToolStripMenuItem->Click += gcnew System::EventHandler(this, &Main::replaceToolStripMenuItem_Click);
			// 
			// goToToolStripMenuItem
			// 
			this->goToToolStripMenuItem->Name = L"goToToolStripMenuItem";
			this->goToToolStripMenuItem->ShortcutKeyDisplayString = L"";
			this->goToToolStripMenuItem->ShortcutKeys = static_cast<System::Windows::Forms::Keys>((System::Windows::Forms::Keys::Control | System::Windows::Forms::Keys::G));
			this->goToToolStripMenuItem->Size = System::Drawing::Size(167, 22);
			this->goToToolStripMenuItem->Text = L"Go To...";
			this->goToToolStripMenuItem->Click += gcnew System::EventHandler(this, &Main::goToToolStripMenuItem_Click);
			// 
			// toolStripSeparator5
			// 
			this->toolStripSeparator5->Name = L"toolStripSeparator5";
			this->toolStripSeparator5->Size = System::Drawing::Size(164, 6);
			// 
			// selectAllToolStripMenuItem
			// 
			this->selectAllToolStripMenuItem->Name = L"selectAllToolStripMenuItem";
			this->selectAllToolStripMenuItem->ShortcutKeyDisplayString = L"Ctrl+A";
			this->selectAllToolStripMenuItem->Size = System::Drawing::Size(167, 22);
			this->selectAllToolStripMenuItem->Text = L"Select All";
			this->selectAllToolStripMenuItem->Click += gcnew System::EventHandler(this, &Main::selectAllToolStripMenuItem_Click);
			// 
			// timeDateToolStripMenuItem
			// 
			this->timeDateToolStripMenuItem->Name = L"timeDateToolStripMenuItem";
			this->timeDateToolStripMenuItem->ShortcutKeyDisplayString = L"";
			this->timeDateToolStripMenuItem->ShortcutKeys = System::Windows::Forms::Keys::F5;
			this->timeDateToolStripMenuItem->Size = System::Drawing::Size(167, 22);
			this->timeDateToolStripMenuItem->Text = L"Time/Date";
			this->timeDateToolStripMenuItem->Click += gcnew System::EventHandler(this, &Main::timeDateToolStripMenuItem_Click);
			// 
			// formatToolStripMenuItem
			// 
			this->formatToolStripMenuItem->DropDownItems->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(2)
			{
				this->wordWrapToolStripMenuItem,
					this->fontToolStripMenuItem
			});
			this->formatToolStripMenuItem->Name = L"formatToolStripMenuItem";
			this->formatToolStripMenuItem->Size = System::Drawing::Size(57, 20);
			this->formatToolStripMenuItem->Text = L"Format";
			// 
			// wordWrapToolStripMenuItem
			// 
			this->wordWrapToolStripMenuItem->Name = L"wordWrapToolStripMenuItem";
			this->wordWrapToolStripMenuItem->Size = System::Drawing::Size(134, 22);
			this->wordWrapToolStripMenuItem->Text = L"Word Wrap";
			this->wordWrapToolStripMenuItem->Click += gcnew System::EventHandler(this, &Main::wordWrapToolStripMenuItem_Click);
			// 
			// fontToolStripMenuItem
			// 
			this->fontToolStripMenuItem->Name = L"fontToolStripMenuItem";
			this->fontToolStripMenuItem->Size = System::Drawing::Size(134, 22);
			this->fontToolStripMenuItem->Text = L"Font...";
			this->fontToolStripMenuItem->Click += gcnew System::EventHandler(this, &Main::fontToolStripMenuItem_Click);
			// 
			// viewToolStripMenuItem
			// 
			this->viewToolStripMenuItem->DropDownItems->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(1)
			{
				this->statusBarToolStripMenuItem
			});
			this->viewToolStripMenuItem->Name = L"viewToolStripMenuItem";
			this->viewToolStripMenuItem->Size = System::Drawing::Size(44, 20);
			this->viewToolStripMenuItem->Text = L"View";
			// 
			// statusBarToolStripMenuItem
			// 
			this->statusBarToolStripMenuItem->Name = L"statusBarToolStripMenuItem";
			this->statusBarToolStripMenuItem->Size = System::Drawing::Size(126, 22);
			this->statusBarToolStripMenuItem->Text = L"Status Bar";
			this->statusBarToolStripMenuItem->Click += gcnew System::EventHandler(this, &Main::statusBarToolStripMenuItem_Click);
			// 
			// helpToolStripMenuItem
			// 
			this->helpToolStripMenuItem->DropDownItems->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(3)
			{
				this->viewHelpToolStripMenuItem,
					this->toolStripSeparator6, this->aboutNopepadToolStripMenuItem
			});
			this->helpToolStripMenuItem->Name = L"helpToolStripMenuItem";
			this->helpToolStripMenuItem->Size = System::Drawing::Size(44, 20);
			this->helpToolStripMenuItem->Text = L"Help";
			// 
			// viewHelpToolStripMenuItem
			// 
			this->viewHelpToolStripMenuItem->Name = L"viewHelpToolStripMenuItem";
			this->viewHelpToolStripMenuItem->Size = System::Drawing::Size(173, 22);
			this->viewHelpToolStripMenuItem->Text = L"View Help";
			this->viewHelpToolStripMenuItem->Click += gcnew System::EventHandler(this, &Main::viewHelpToolStripMenuItem_Click);
			// 
			// toolStripSeparator6
			// 
			this->toolStripSeparator6->Name = L"toolStripSeparator6";
			this->toolStripSeparator6->Size = System::Drawing::Size(170, 6);
			// 
			// aboutNopepadToolStripMenuItem
			// 
			this->aboutNopepadToolStripMenuItem->Name = L"aboutNopepadToolStripMenuItem";
			this->aboutNopepadToolStripMenuItem->Size = System::Drawing::Size(173, 22);
			this->aboutNopepadToolStripMenuItem->Text = L"About Nopepad";
			this->aboutNopepadToolStripMenuItem->Click += gcnew System::EventHandler(this, &Main::aboutNopepadToolStripMenuItem_Click);
			// 
			// statusStrip1
			// 
			this->statusStrip1->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(1)
			{
				this->toolStripStatusLabel1
			});
			this->statusStrip1->Location = System::Drawing::Point(0, 357);
			this->statusStrip1->Name = L"statusStrip1";
			this->statusStrip1->Size = System::Drawing::Size(684, 22);
			this->statusStrip1->TabIndex = 2;
			this->statusStrip1->Text = L"statusStrip1";
			this->statusStrip1->Visible = false;
			this->statusStrip1->VisibleChanged += gcnew System::EventHandler(this, &Main::statusStrip1_VisibleChanged);
			// 
			// toolStripStatusLabel1
			// 
			this->toolStripStatusLabel1->Name = L"toolStripStatusLabel1";
			this->toolStripStatusLabel1->Size = System::Drawing::Size(62, 17);
			this->toolStripStatusLabel1->Text = L"Ln 1, Col 1";
			// 
			// timer1
			// 
			this->timer1->Enabled = true;
			this->timer1->Interval = 25;
			this->timer1->Tick += gcnew System::EventHandler(this, &Main::timer1_Tick);
			// 
			// Main
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(684, 379);
			this->Controls->Add(this->richTextBox1);
			this->Controls->Add(this->statusStrip1);
			this->Controls->Add(this->menuStrip1);
			this->MainMenuStrip = this->menuStrip1;
			this->Name = L"Main";
			this->Text = L"Untitled";
			this->FormClosing += gcnew System::Windows::Forms::FormClosingEventHandler(this, &Main::Main_FormClosing);
			this->Load += gcnew System::EventHandler(this, &Main::Main_Load);
			this->menuStrip1->ResumeLayout(false);
			this->menuStrip1->PerformLayout();
			this->statusStrip1->ResumeLayout(false);
			this->statusStrip1->PerformLayout();
			this->ResumeLayout(false);
			this->PerformLayout();

		}
		#pragma endregion


		const String^ lineTxt = gcnew String("Ln ");
		const String^ columnTxt = gcnew String(", Col ");

		bool originalTextEmpty = true;
		bool openedFile = false;
		bool saved = false;
		bool clipContTxt = false;
		bool closed = false;

		String^ openedFileLoc = "";

		PrintDocument^ docToPrint = gcnew PrintDocument();

		System::Void Main_Load(System::Object^ sender, System::EventArgs^ e)
		{
			docToPrint->PrintPage += gcnew System::Drawing::Printing::PrintPageEventHandler(this, &Nopepad::Main::document_PrintPage);

			Threading::Thread^ thisThread = gcnew Threading::Thread(gcnew Threading::ThreadStart(this, &Main::clipboardContainsText));
			thisThread->ApartmentState = Threading::ApartmentState::STA;
			thisThread->Start();
		}

		int getRTBColumn()
		{
			int index = richTextBox1->SelectionStart;
			int line = richTextBox1->GetLineFromCharIndex(index);

			int firstChar = richTextBox1->GetFirstCharIndexFromLine(line);
			int column = index - firstChar;

			return column;
		}

		int getRTBLine()
		{
			int index = richTextBox1->SelectionStart;
			int line = richTextBox1->GetLineFromCharIndex(index);

			return line;
		}

		String^ fileName = String::Empty;
		int result = -1;

		void showSFDialog()
		{
			SaveFileDialog^ sfd = gcnew SaveFileDialog;
			sfd->Filter = "Text File (*.txt)|*.txt|All Files (*.*)|*.*";
			System::Windows::Forms::DialogResult sfdDr = sfd->ShowDialog();

			fileName = sfd->FileName;
			result = safe_cast<int>(sfdDr);
		}

		void showOFDialog()
		{
			OpenFileDialog^ ofd = gcnew OpenFileDialog;
			ofd->Filter = "Text File (*.txt)|*.txt|All Files (*.*)|*.*";
			ofd->CheckFileExists = true;

			System::Windows::Forms::DialogResult ofdDr = ofd->ShowDialog();

			fileName = ofd->FileName;
			result = safe_cast<int>(ofdDr);
		}

		void save(bool dialogCheck)
		{
			String^ interFileName = fileName;
			fileName = String::Empty;

			System::Windows::Forms::DialogResult diagResult = safe_cast<System::Windows::Forms::DialogResult>(result);
			result = -1;

			if (!dialogCheck || diagResult == System::Windows::Forms::DialogResult::OK)
			{
				File::WriteAllText(interFileName, richTextBox1->Text);

				openedFileLoc = interFileName;

				int slashIndex = openedFileLoc->LastIndexOf("\\") + 1;
				Main::Text = openedFileLoc->Substring(slashIndex);
			}
		}

		bool saveWithSaveFileDialog(bool checkSaved, bool saveAs, bool askSure)
		{
			if (checkSaved && saved || (originalTextEmpty && String::IsNullOrEmpty(richTextBox1->Text)))
			{
				return true;
			}
			else
			{
				if (saveAs)
				{
					System::Windows::Forms::DialogResult dr = (askSure ? MessageBox::Show("Do you want to save changes to " + Main::Text + "?", "Notepad", System::Windows::Forms::MessageBoxButtons::YesNoCancel) : System::Windows::Forms::DialogResult::Yes);

					if (dr == System::Windows::Forms::DialogResult::Yes)
					{
						Threading::Thread^ thisThread = gcnew Threading::Thread(gcnew Threading::ThreadStart(this, &Main::showSFDialog));
						thisThread->ApartmentState = Threading::ApartmentState::STA;
						thisThread->Start();
						thisThread->Join();

						if (fileName == String::Empty || result == -1) return false;

						save(true);
					}
					else if (dr == System::Windows::Forms::DialogResult::Cancel)
					{
						return false;
					}

					return true;
				}
				else
				{
					save(false);
					return true;
				}
			}

			return false;
		}

		System::Void newToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e)
		{
			bool yes = saveWithSaveFileDialog(true, !saved, true);

			if (yes)
			{
				Main::Text = "Untitled";
				richTextBox1->Text = "";
				saved = false;
				originalTextEmpty = true;
				openedFile = false;
			}
		}

		System::Void openToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e)
		{
			saveWithSaveFileDialog(true, !saved, true);

			Threading::Thread^ thisThread = gcnew Threading::Thread(gcnew Threading::ThreadStart(this, &Main::showOFDialog));
			thisThread->ApartmentState = Threading::ApartmentState::STA;
			thisThread->Start();
			thisThread->Join();

			String^ interFileName = fileName;
			fileName = String::Empty;

			System::Windows::Forms::DialogResult diagResult = safe_cast<System::Windows::Forms::DialogResult>(result);
			result = -1;

			if (diagResult == System::Windows::Forms::DialogResult::OK)
			{
				StreamReader^ din = File::OpenText(interFileName);

				StringBuilder^ sb = gcnew StringBuilder();
				String^ str;
				while ((str = din->ReadLine()) != nullptr)
				{
					sb->AppendLine(str);
				}

				din->Close();
				richTextBox1->Text = sb->ToString();
				openedFile = true;
				openedFileLoc = interFileName;
				int slashIndex = openedFileLoc->LastIndexOf("\\") + 1;
				Main::Text = openedFileLoc->Substring(slashIndex);
			}
		}

		System::Void saveToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e)
		{
			if (!saved)
			{
				if (openedFile)
				{
					File::WriteAllText(openedFileLoc, richTextBox1->Text);
					saved = true;
				}
				else
				{
					saveWithSaveFileDialog(false, !saved, false);
				}

				saved = false;
			}
		}

		System::Void saveAsToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e)
		{
			saveWithSaveFileDialog(false, true, false);

			saved = true;
			originalTextEmpty = String::IsNullOrEmpty(richTextBox1->Text);
			openedFile = true;
		}

		System::Void pageSetupToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e)
		{
			PageSetupDialog^ psd = gcnew PageSetupDialog();

			psd->Document = docToPrint;

			psd->PageSettings = gcnew System::Drawing::Printing::PageSettings();

			psd->PrinterSettings = gcnew System::Drawing::Printing::PrinterSettings();

			psd->ShowNetwork = false;

			System::Windows::Forms::DialogResult result = psd->ShowDialog();

			if (result == System::Windows::Forms::DialogResult::OK)
			{
				docToPrint->DefaultPageSettings = psd->PageSettings;
				docToPrint->PrinterSettings = psd->PrinterSettings;
			}
		}

		void document_PrintPage(Object^ sender, PrintPageEventArgs^ ev)
		{
			System::Drawing::Font^ printFont = gcnew System::Drawing::Font("Arial", 10);

			int linesPerPage;
			int count = 0;
			float leftMargin = (float)ev->MarginBounds.Left;
			float topMargin = (float)ev->MarginBounds.Top;
			String^ line = nullptr;

			linesPerPage = ev->MarginBounds.Height / Convert::ToInt32(printFont->GetHeight(ev->Graphics));

			for each (line in richTextBox1->Lines)
			{
				if (count > linesPerPage) break;
				ev->Graphics->DrawString(line, printFont, Brushes::Black, leftMargin, topMargin + (count * printFont->GetHeight(ev->Graphics)), gcnew StringFormat);
				count++;
			}

			if (count < richTextBox1->Lines->Length)
				ev->HasMorePages = true;
			else
				ev->HasMorePages = false;
		}


		System::Void printToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e)
		{
			PrintDialog^ printDialog = gcnew PrintDialog();

			printDialog->AllowSomePages = true;

			printDialog->ShowHelp = true;

			printDialog->Document = docToPrint;

			System::Windows::Forms::DialogResult result = printDialog->ShowDialog();
			
			if (result == System::Windows::Forms::DialogResult::OK)
			{
				docToPrint->Print();
			}
		}

		System::Void exitToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e)
		{
			this->Close();
		}

		System::Void undoToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e)
		{
			richTextBox1->Undo();
		}

		System::Void cutToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e)
		{
			richTextBox1->Cut();
		}

		System::Void copyToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e)
		{
			richTextBox1->Copy();
		}

		System::Void pasteToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e)
		{
			richTextBox1->Paste();
		}

		System::Void deleteToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e)
		{
			richTextBox1->SelectedText = "";
		}

		Find^ frmFind = nullptr;
		bool findReady = false;
		String^ findSearchText = "";

		System::Void couldNotFind(String^ text)
		{
			System::Media::SystemSounds::Beep->Play();
			MessageBox::Show("Could not find \"" + text + "\"");
		}

		System::Void doSearch(String^ text, bool caseSensitive, bool downDirection)
		{
			if (richTextBox1->Text->Length > 0)
			{
				String^ rtb;
				bool didSubstr = false;
				bool skipToNotFound = false;

				if (richTextBox1->SelectionStart < richTextBox1->Text->Length)
				{
					if (downDirection)
					{
						rtb = richTextBox1->Text->Substring(richTextBox1->SelectionStart + richTextBox1->SelectionLength);
					}
					else
					{
						rtb = richTextBox1->Text->Substring(0, richTextBox1->SelectionStart);
					}

					didSubstr = true;
				}
				else
				{
					skipToNotFound = true;
				}

				int index = -1;
				if (!skipToNotFound)
				{
					if (downDirection) index = rtb->IndexOf(text, (caseSensitive ? StringComparison::CurrentCulture : StringComparison::CurrentCultureIgnoreCase));
					else index = rtb->LastIndexOf(text, (caseSensitive ? StringComparison::CurrentCulture : StringComparison::CurrentCultureIgnoreCase));

					if (didSubstr && index != -1)
					{
						if (downDirection)
						{
							index += richTextBox1->SelectionStart + richTextBox1->SelectionLength;
						}

					}
				}

				if (index > -1)
				{
					richTextBox1->Focus();
					richTextBox1->SelectionStart = index;
					richTextBox1->SelectionLength = text->Length;
					richTextBox1->ScrollToCaret();
				}
				else
				{
					couldNotFind(text);
				}
			}
		}

		System::Void doReplace(bool all, String^ textToReplace, String^ replaceWith)
		{
			if (richTextBox1->Text->Length > 0)
			{
				String^ rtb;

				if (all)
				{
					richTextBox1->Text->Replace(textToReplace, replaceWith);
				}
				else
				{
					bool didSubstr = false;

					if (richTextBox1->SelectionStart < richTextBox1->Text->Length)
					{
						rtb = richTextBox1->Text->Substring(richTextBox1->SelectionStart + 1);

						didSubstr = true;
					}
					else
					{
						rtb = richTextBox1->Text;
					}

					int index = rtb->IndexOf(textToReplace, (frmReplace->cbMatchCase->Checked ? StringComparison::CurrentCulture : StringComparison::CurrentCultureIgnoreCase));
					if (didSubstr && index != -1) index += richTextBox1->SelectionStart + 1;

					if (index > -1)
					{
						StringBuilder^ sb = gcnew StringBuilder(richTextBox1->Text);
						sb->Remove(index, textToReplace->Length);
						sb->Insert(index, replaceWith);

						richTextBox1->Text = sb->ToString();

						richTextBox1->Focus();
						richTextBox1->SelectionStart = index;
						richTextBox1->SelectionLength = replaceWith->Length;
						richTextBox1->ScrollToCaret();
					}
					else
					{
						couldNotFind(textToReplace);
					}
				}
			}
			else
			{
				couldNotFind(textToReplace);
			}
		}

		System::Void findToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e)
		{
			frmFind = gcnew Find();
			frmFind->TopMost = true;
			frmFind->Show();
			findReady = true;
		}

		bool lastCaseSensitive = false;
		bool lastDownDirection = true;

		System::Void findNextToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e)
		{
			if (findSearchText == "")
			{
				findToolStripMenuItem_Click(nullptr, nullptr);
			}
			else
			{
				doSearch(findSearchText, lastCaseSensitive, lastDownDirection);
			}
		}

		Replace^ frmReplace = nullptr;
		bool replaceReady = false;
		String^ replaceSearchText = "";
		int mode = 0;
		int replaceLastIndex = 0;

		System::Void replaceToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e)
		{
			frmReplace = gcnew Replace();
			frmReplace->TopMost = true;
			frmReplace->Show();
			replaceReady = true;
		}

		GoTo^ frmGoTo = nullptr;
		bool gotoReady = false;

		System::Void resetLineNum()
		{
			frmGoTo->lineNum = -1;
		}

		System::Void goToToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e)
		{
			gotoReady = true;
			frmGoTo = gcnew GoTo();
			frmGoTo->ShowDialog();
		}

		System::Void clipboardContainsText()
		{
			while (!closed)
			{
				clipContTxt = Clipboard::ContainsText();
				Thread::Sleep(500);
			}
		}

		System::Void timer1_Tick(System::Object^  sender, System::EventArgs^  e)
		{
			bool selLeng = richTextBox1->SelectionLength > 0;

			cutToolStripMenuItem->Enabled = selLeng;
			copyToolStripMenuItem->Enabled = selLeng;
			pasteToolStripMenuItem->Enabled = clipContTxt;
			deleteToolStripMenuItem->Enabled = selLeng;

			if (replaceReady)
			{
				if (frmReplace != nullptr)
				{
					if (frmReplace->searchText != "")
					{
						findSearchText = frmReplace->searchText;
						frmReplace->searchText = "";

						mode = frmReplace->mode;

						replaceSearchText = frmReplace->replaceText;
						frmReplace->replaceText = "";

						switch (mode)
						{
							case 0:
								doSearch(findSearchText, frmReplace->cbMatchCase->Checked, true);
								break;
							case 1:
								doReplace(false, findSearchText, replaceSearchText);
								break;
							case 2:
								doReplace(false, findSearchText, replaceSearchText);
								break;
						}
					}
				}
				else replaceReady = false;
			}

			if (gotoReady)
			{
				if (frmGoTo != nullptr && frmGoTo->Created)
				{
					if (frmGoTo->lineNum > 0)
					{
						if (frmGoTo->lineNum <= richTextBox1->Lines->Length)
						{
							int charIndex = richTextBox1->GetFirstCharIndexFromLine(frmGoTo->lineNum);
							if (charIndex < 1) charIndex = 1;
							richTextBox1->Focus();
							richTextBox1->Select(charIndex - 1, 0);
							richTextBox1->ScrollToCaret();

							frmGoTo->Invoke(gcnew Action(this, &Main::resetLineNum));

							frmGoTo->Close();
						}
					}
				}
				else gotoReady = false;
			}

			if (findReady)
			{
				if (frmFind != nullptr)
				{
					if (frmFind->searchText != "")
					{
						findSearchText = frmFind->searchText;

						frmFind->searchText = "";

						lastCaseSensitive = frmFind->cbMatchCase->Checked;

						lastDownDirection = frmFind->rbDown->Checked;

						doSearch(findSearchText, lastCaseSensitive, lastDownDirection);
					}
				}
				else findReady = false;
			}

			if (statusStrip1->Visible)
			{
				int line = getRTBLine();

				int column = getRTBColumn();

				array<String^>^ strings = { const_cast<String^>(lineTxt), gcnew String(std::to_string(line + 1).c_str()), const_cast<String^>(columnTxt), gcnew String(std::to_string(column + 1).c_str()) };

				toolStripStatusLabel1->Text = String::Concat(strings);
			}
		}

		System::Void selectAllToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e)
		{
			richTextBox1->SelectAll();
		}

		System::Void timeDateToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e)
		{
			DateTime^ dateTime = DateTime::Now;
			String^ strDateTime = dateTime->ToString("hh:mm tt MM/dd/yyyy");
			if (richTextBox1->SelectionLength > 0) richTextBox1->Text->Remove(richTextBox1->SelectionStart, richTextBox1->SelectionLength);
			richTextBox1->Text = richTextBox1->Text->Insert(richTextBox1->SelectionStart, strDateTime);
		}

		System::Void wordWrapToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e)
		{
			bool value = !wordWrapToolStripMenuItem->Checked;
			wordWrapToolStripMenuItem->Checked = value;
			richTextBox1->WordWrap = value;
		}

		FontDialog^ fd = nullptr;

		System::Void fontApply(System::Object^ sender, System::EventArgs^ e)
		{
			richTextBox1->Font = fd->Font;
		}

		System::Void fontToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e)
		{
			fd = gcnew FontDialog();
			fd->Apply += gcnew System::EventHandler(this, &Main::fontApply);
			fd->ShowApply = true;
			System::Drawing::Font^ oldFont = richTextBox1->Font;
			fd->Font = oldFont;
			System::Windows::Forms::DialogResult result = fd->ShowDialog();

			if (result == System::Windows::Forms::DialogResult::OK)
			{
				fontApply(this->fontToolStripMenuItem, gcnew System::EventArgs);
			}
			else if (result == System::Windows::Forms::DialogResult::Cancel)
			{
				richTextBox1->Font = oldFont;
			}
		}

		System::Void toggleStatusBar()
		{
			bool value = !statusStrip1->Visible;
			statusStrip1->Visible = value;
			statusBarToolStripMenuItem->Checked = value;
		}

		System::Void statusBarToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e)
		{
			toggleStatusBar();
		}

		Thread^ helpThread = nullptr;

		System::Void openHelp()
		{
			Application::Run(gcnew NPHelp());
		}

		System::Void viewHelpToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e)
		{
			helpThread = gcnew Threading::Thread(gcnew Threading::ThreadStart(this, &Main::openHelp));
			helpThread->ApartmentState = Threading::ApartmentState::STA;
			helpThread->Start();
		}

		System::Void aboutNopepadToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e)
		{
			About ^ form = gcnew About;
			form->ShowDialog();
		}

		System::Void statusStrip1_VisibleChanged(System::Object^  sender, System::EventArgs^  e)
		{
			if (statusStrip1->Visible)
			{
				richTextBox1->Height -= 22;
			}
			else
			{
				richTextBox1->Height += 22;
			}
		}

		private: System::Void richTextBox1_TextChanged(System::Object^  sender, System::EventArgs^  e)
		{
			if ((originalTextEmpty && richTextBox1->Text == "") || !originalTextEmpty) saved = false;

			if (richTextBox1->Text != "")
			{
				undoToolStripMenuItem->Enabled = true;
				findToolStripMenuItem->Enabled = true;
				findNextToolStripMenuItem->Enabled = true;
			}
			else
			{
				findToolStripMenuItem->Enabled = false;
				cutToolStripMenuItem->Enabled = false;
				copyToolStripMenuItem->Enabled = false;
				deleteToolStripMenuItem->Enabled = false;
				findToolStripMenuItem->Enabled = false;
				findNextToolStripMenuItem->Enabled = false;
			}
		}
		private: System::Void Main_FormClosing(System::Object^  sender, System::Windows::Forms::FormClosingEventArgs^  e)
		{
			closed = true;

			bool close = saveWithSaveFileDialog(true, !saved, true);
			if (!close) e->Cancel = true;
		}
	};
}
