#pragma once

namespace Nopepad
{

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;
	using namespace System::IO;

	/// <summary>
	/// Summary for NPHelp
	/// </summary>
	public ref class NPHelp : public System::Windows::Forms::Form
	{	
		public:
		[STAThread]
		NPHelp(void)
		{
			InitializeComponent();
			//
			//TODO: Add the constructor code here
			//
		}

		protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~NPHelp()
		{
			if (components)
			{
				delete components;
			}
		}
		private: System::Windows::Forms::WebBrowser^  webBrowser1;
		protected:

		private:
		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container ^components;

		#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->webBrowser1 = (gcnew System::Windows::Forms::WebBrowser());
			this->SuspendLayout();
			// 
			// webBrowser1
			// 
			this->webBrowser1->Dock = System::Windows::Forms::DockStyle::Fill;
			this->webBrowser1->Location = System::Drawing::Point(0, 0);
			this->webBrowser1->MinimumSize = System::Drawing::Size(20, 20);
			this->webBrowser1->Name = L"webBrowser1";
			this->webBrowser1->Size = System::Drawing::Size(753, 493);
			this->webBrowser1->TabIndex = 0;
			// 
			// NPHelp
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(753, 493);
			this->Controls->Add(this->webBrowser1);
			this->FormBorderStyle = System::Windows::Forms::FormBorderStyle::FixedDialog;
			this->Name = L"NPHelp";
			this->Text = L"Help";
			this->Load += gcnew System::EventHandler(this, &NPHelp::NPHelp_Load);
			this->ResumeLayout(false);

		}
		#pragma endregion

		private: System::Void NPHelp_Load(System::Object^  sender, System::EventArgs^  e)
		{
			webBrowser1->ScriptErrorsSuppressed = true;
			
			webBrowser1->Navigate("http://windows.microsoft.com/en-us/windows/notepad-faq");
		}
	};
}
