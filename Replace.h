#pragma once

namespace Nopepad
{

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// Summary for Replace
	/// </summary>
	public ref class Replace : public System::Windows::Forms::Form
	{
		public:
		Replace(void)
		{
			InitializeComponent();
			//
			//TODO: Add the constructor code here
			//
		}

		protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~Replace()
		{
			if (components)
			{
				delete components;
			}
		}
		private: System::Windows::Forms::Button^  btnFindNext;
		protected:
		private: System::Windows::Forms::Button^  btnCancel;
		public: System::Windows::Forms::CheckBox^  cbMatchCase;
		private:
		private: System::Windows::Forms::Label^  label1;
		public:
		private: System::Windows::Forms::TextBox^  tbFindWhat;
		private: System::Windows::Forms::Button^  btnReplace;
		private: System::Windows::Forms::Button^  btnReplaceAll;
		private: System::Windows::Forms::Label^  label2;
		private: System::Windows::Forms::TextBox^  tbReplace;


		private:
		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container ^components;

		#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->btnFindNext = (gcnew System::Windows::Forms::Button());
			this->btnCancel = (gcnew System::Windows::Forms::Button());
			this->cbMatchCase = (gcnew System::Windows::Forms::CheckBox());
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->tbFindWhat = (gcnew System::Windows::Forms::TextBox());
			this->btnReplace = (gcnew System::Windows::Forms::Button());
			this->btnReplaceAll = (gcnew System::Windows::Forms::Button());
			this->label2 = (gcnew System::Windows::Forms::Label());
			this->tbReplace = (gcnew System::Windows::Forms::TextBox());
			this->SuspendLayout();
			// 
			// btnFindNext
			// 
			this->btnFindNext->Enabled = false;
			this->btnFindNext->Location = System::Drawing::Point(320, 10);
			this->btnFindNext->Name = L"btnFindNext";
			this->btnFindNext->Size = System::Drawing::Size(75, 23);
			this->btnFindNext->TabIndex = 4;
			this->btnFindNext->Text = L"Find Next";
			this->btnFindNext->UseVisualStyleBackColor = true;
			this->btnFindNext->Click += gcnew System::EventHandler(this, &Replace::btnFindNext_Click);
			// 
			// btnCancel
			// 
			this->btnCancel->DialogResult = System::Windows::Forms::DialogResult::Cancel;
			this->btnCancel->Location = System::Drawing::Point(322, 97);
			this->btnCancel->Name = L"btnCancel";
			this->btnCancel->Size = System::Drawing::Size(75, 23);
			this->btnCancel->TabIndex = 7;
			this->btnCancel->Text = L"Cancel";
			this->btnCancel->UseVisualStyleBackColor = true;
			this->btnCancel->Click += gcnew System::EventHandler(this, &Replace::btnCancel_Click);
			// 
			// cbMatchCase
			// 
			this->cbMatchCase->AutoSize = true;
			this->cbMatchCase->Location = System::Drawing::Point(14, 103);
			this->cbMatchCase->Name = L"cbMatchCase";
			this->cbMatchCase->Size = System::Drawing::Size(83, 17);
			this->cbMatchCase->TabIndex = 3;
			this->cbMatchCase->Text = L"Match Case";
			this->cbMatchCase->UseVisualStyleBackColor = true;
			this->cbMatchCase->CheckedChanged += gcnew System::EventHandler(this, &Replace::cbMatchCase_CheckedChanged);
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->Location = System::Drawing::Point(11, 12);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(59, 13);
			this->label1->TabIndex = 6;
			this->label1->Text = L"Find what: ";
			// 
			// tbFindWhat
			// 
			this->tbFindWhat->Location = System::Drawing::Point(95, 12);
			this->tbFindWhat->Name = L"tbFindWhat";
			this->tbFindWhat->Size = System::Drawing::Size(219, 20);
			this->tbFindWhat->TabIndex = 1;
			this->tbFindWhat->TextChanged += gcnew System::EventHandler(this, &Replace::tbFindWhat_TextChanged);
			// 
			// btnReplace
			// 
			this->btnReplace->Enabled = false;
			this->btnReplace->Location = System::Drawing::Point(322, 39);
			this->btnReplace->Name = L"btnReplace";
			this->btnReplace->Size = System::Drawing::Size(75, 23);
			this->btnReplace->TabIndex = 5;
			this->btnReplace->Text = L"Replace";
			this->btnReplace->UseVisualStyleBackColor = true;
			this->btnReplace->Click += gcnew System::EventHandler(this, &Replace::btnReplace_Click);
			// 
			// btnReplaceAll
			// 
			this->btnReplaceAll->Enabled = false;
			this->btnReplaceAll->Location = System::Drawing::Point(320, 68);
			this->btnReplaceAll->Name = L"btnReplaceAll";
			this->btnReplaceAll->Size = System::Drawing::Size(75, 23);
			this->btnReplaceAll->TabIndex = 6;
			this->btnReplaceAll->Text = L"Replace All";
			this->btnReplaceAll->UseVisualStyleBackColor = true;
			this->btnReplaceAll->Click += gcnew System::EventHandler(this, &Replace::btnReplaceAll_Click);
			// 
			// label2
			// 
			this->label2->AutoSize = true;
			this->label2->Location = System::Drawing::Point(11, 44);
			this->label2->Name = L"label2";
			this->label2->Size = System::Drawing::Size(78, 13);
			this->label2->TabIndex = 13;
			this->label2->Text = L"Replace With: ";
			// 
			// tbReplace
			// 
			this->tbReplace->Location = System::Drawing::Point(95, 41);
			this->tbReplace->Name = L"tbReplace";
			this->tbReplace->Size = System::Drawing::Size(219, 20);
			this->tbReplace->TabIndex = 2;
			// 
			// Replace
			// 
			this->AcceptButton = this->btnFindNext;
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->CancelButton = this->btnCancel;
			this->ClientSize = System::Drawing::Size(409, 155);
			this->Controls->Add(this->tbReplace);
			this->Controls->Add(this->label2);
			this->Controls->Add(this->btnReplaceAll);
			this->Controls->Add(this->btnReplace);
			this->Controls->Add(this->cbMatchCase);
			this->Controls->Add(this->tbFindWhat);
			this->Controls->Add(this->label1);
			this->Controls->Add(this->btnCancel);
			this->Controls->Add(this->btnFindNext);
			this->FormBorderStyle = System::Windows::Forms::FormBorderStyle::FixedDialog;
			this->MaximizeBox = false;
			this->MinimizeBox = false;
			this->Name = L"Replace";
			this->Text = L"Replace";
			this->ResumeLayout(false);
			this->PerformLayout();

		}
		#pragma endregion

		public: bool matchCase = false;
		public: int mode = 0;
		public: String^ searchText = "", ^replaceText = "";

		private: System::Void btnFindNext_Click(System::Object^  sender, System::EventArgs^  e)
		{
			mode = 0;
			searchText = tbFindWhat->Text;
		}

		private: System::Void btnReplace_Click(System::Object^  sender, System::EventArgs^  e)
		{
			mode = 1;
			searchText = tbFindWhat->Text;
			replaceText = tbReplace->Text;
		}

		private: System::Void btnReplaceAll_Click(System::Object^  sender, System::EventArgs^  e)
		{
			mode = 2;
			searchText = tbFindWhat->Text;
			replaceText = tbReplace->Text;
		}

		private: System::Void btnCancel_Click(System::Object^  sender, System::EventArgs^  e)
		{
			this->Close();
		}

		private: System::Void cbMatchCase_CheckedChanged(System::Object^  sender, System::EventArgs^  e)
		{
			matchCase = cbMatchCase->Checked;
		}

		private: System::Void tbFindWhat_TextChanged(System::Object^  sender, System::EventArgs^  e)
		{
			bool enabled = !String::IsNullOrEmpty(tbFindWhat->Text);

			btnFindNext->Enabled = enabled;
			btnReplace->Enabled = enabled;
			btnReplaceAll->Enabled = enabled;
		}
	};
}
