#pragma once

#include <iostream>
#include <memory>

namespace Nopepad
{

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Collections::Generic;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// Summary for GoTo
	/// </summary>
	public ref class GoTo : public System::Windows::Forms::Form
	{
		public:
		GoTo(void)
		{
			InitializeComponent();
			//
			//TODO: Add the constructor code here
			//
		}

		protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~GoTo()
		{
			if (components)
			{
				delete components;
			}
		}
		private: System::Windows::Forms::Label^  label1;
		private: System::Windows::Forms::TextBox^  tbLineNum;
		protected:

		private: System::Windows::Forms::Button^  btnGoTo;
		private: System::Windows::Forms::Button^  btnCancel;

		private:
		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container ^components;

		#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->tbLineNum = (gcnew System::Windows::Forms::TextBox());
			this->btnGoTo = (gcnew System::Windows::Forms::Button());
			this->btnCancel = (gcnew System::Windows::Forms::Button());
			this->SuspendLayout();
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->Location = System::Drawing::Point(12, 9);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(68, 13);
			this->label1->TabIndex = 0;
			this->label1->Text = L"Line number:";
			// 
			// tbLineNum
			// 
			this->tbLineNum->Location = System::Drawing::Point(13, 26);
			this->tbLineNum->Name = L"tbLineNum";
			this->tbLineNum->Size = System::Drawing::Size(223, 20);
			this->tbLineNum->TabIndex = 1;
			this->tbLineNum->TextChanged += gcnew System::EventHandler(this, &GoTo::tbLineNum_TextChanged);
			// 
			// btnGoTo
			// 
			this->btnGoTo->Enabled = false;
			this->btnGoTo->Location = System::Drawing::Point(80, 52);
			this->btnGoTo->Name = L"btnGoTo";
			this->btnGoTo->Size = System::Drawing::Size(75, 23);
			this->btnGoTo->TabIndex = 2;
			this->btnGoTo->Text = L"Go To";
			this->btnGoTo->UseVisualStyleBackColor = true;
			this->btnGoTo->Click += gcnew System::EventHandler(this, &GoTo::btnGoTo_Click);
			// 
			// btnCancel
			// 
			this->btnCancel->DialogResult = System::Windows::Forms::DialogResult::Cancel;
			this->btnCancel->Location = System::Drawing::Point(161, 52);
			this->btnCancel->Name = L"btnCancel";
			this->btnCancel->Size = System::Drawing::Size(75, 23);
			this->btnCancel->TabIndex = 3;
			this->btnCancel->Text = L"Cancel";
			this->btnCancel->UseVisualStyleBackColor = true;
			this->btnCancel->Click += gcnew System::EventHandler(this, &GoTo::btnCancel_Click);
			// 
			// GoTo
			// 
			this->AcceptButton = this->btnGoTo;
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->CancelButton = this->btnCancel;
			this->ClientSize = System::Drawing::Size(250, 84);
			this->Controls->Add(this->btnCancel);
			this->Controls->Add(this->btnGoTo);
			this->Controls->Add(this->tbLineNum);
			this->Controls->Add(this->label1);
			this->FormBorderStyle = System::Windows::Forms::FormBorderStyle::FixedDialog;
			this->MaximizeBox = false;
			this->MinimizeBox = false;
			this->Name = L"GoTo";
			this->ShowInTaskbar = false;
			this->Text = L"Go To Line";
			this->Load += gcnew System::EventHandler(this, &GoTo::GoTo_Load);
			this->ResumeLayout(false);
			this->PerformLayout();

		}
		#pragma endregion

		public: int lineNum = -1;

		private: System::Void GoTo_Load(System::Object^  sender, System::EventArgs^  e)
		{
			Form::AcceptButton = btnGoTo;
		}

		private: System::Void btnGoTo_Click(System::Object^  sender, System::EventArgs^  e)
		{
			lineNum = int::Parse(tbLineNum->Text);
		}

		private: System::Void tbLineNum_TextChanged(System::Object^  sender, System::EventArgs^  e)
		{
			if (tbLineNum->Text == "")
			{
				btnGoTo->Enabled = false;
			}
			else
			{
				btnGoTo->Enabled = true;

				List<wchar_t>^ s = gcnew List<wchar_t>(tbLineNum->Text->ToCharArray());
				bool changed = false;

				for (int i = 0; i < s->Count; i++)
				{
					int iChar = (int)s[i];
					String^ sChar = iChar.ToString();

					if (iChar < 48 || iChar > 57)
					{
						s->RemoveAt(i);
						i--;
						changed = true;
					}
				}

				if (changed)
				{
					int selected = tbLineNum->SelectionStart;
					tbLineNum->Text = String::Concat(s);
					tbLineNum->SelectionStart = selected - 1;
				}
			}
		}

		private: System::Void btnCancel_Click(System::Object^  sender, System::EventArgs^  e)
		{
			this->Close();
		}
	};
}
